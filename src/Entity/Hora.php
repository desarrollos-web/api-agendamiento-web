<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Hora
 *
 * @ORM\Table(name="hora", indexes={@ORM\Index(name="FK_hora_general", columns={"estado"})})
 * @ORM\Entity
 */
class Hora
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="hora", type="string", length=10, nullable=true, options={"default"="NULL"})
     */
    private $hora = 'NULL';

    /**
     * @var \General
     *
     * @ORM\ManyToOne(targetEntity="General")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="estado", referencedColumnName="id")
     * })
     */
    private $estado;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getHora(): ?string
    {
        return $this->hora;
    }

    public function setHora(?string $hora): self
    {
        $this->hora = $hora;

        return $this;
    }

    public function getEstado(): ?General
    {
        return $this->estado;
    }

    public function setEstado(?General $estado): self
    {
        $this->estado = $estado;

        return $this;
    }


}
