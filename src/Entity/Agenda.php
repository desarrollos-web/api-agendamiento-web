<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Agenda
 *
 * @ORM\Table(name="agenda", indexes={@ORM\Index(name="FK_agenda_tecnico", columns={"tecnico_id"}), @ORM\Index(name="FK_agenda_hora", columns={"hora_id"}), @ORM\Index(name="FK_agenda_vehiculo", columns={"vehiculo_id"})})
 * @ORM\Entity
 */
class Agenda
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="fecha", type="string", length=20, nullable=true, options={"default"="NULL"})
     */
    private $fecha = 'NULL';

    /**
     * @var string|null
     *
     * @ORM\Column(name="observacion", type="string", length=150, nullable=true, options={"default"="NULL"})
     */
    private $observacion = 'NULL';

    /**
     * @var string|null
     *
     * @ORM\Column(name="servicio", type="string", length=100, nullable=true, options={"default"="NULL"})
     */
    private $servicio = 'NULL';

    /**
     * @var \Hora
     *
     * @ORM\ManyToOne(targetEntity="Hora")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="hora_id", referencedColumnName="id")
     * })
     */
    private $hora;

    /**
     * @var \Tecnico
     *
     * @ORM\ManyToOne(targetEntity="Tecnico")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tecnico_id", referencedColumnName="id")
     * })
     */
    private $tecnico;

    /**
     * @var \Vehiculo
     *
     * @ORM\ManyToOne(targetEntity="Vehiculo")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="vehiculo_id", referencedColumnName="id")
     * })
     */
    private $vehiculo;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFecha(): ?string
    {
        return $this->fecha;
    }

    public function setFecha(?string $fecha): self
    {
        $this->fecha = $fecha;

        return $this;
    }

    public function getObservacion(): ?string
    {
        return $this->observacion;
    }

    public function setObservacion(?string $observacion): self
    {
        $this->observacion = $observacion;

        return $this;
    }

    public function getServicio(): ?string
    {
        return $this->servicio;
    }

    public function setServicio(?string $servicio): self
    {
        $this->servicio = $servicio;

        return $this;
    }

    public function getHora(): ?Hora
    {
        return $this->hora;
    }

    public function setHora(?Hora $hora): self
    {
        $this->hora = $hora;

        return $this;
    }

    public function getTecnico(): ?Tecnico
    {
        return $this->tecnico;
    }

    public function setTecnico(?Tecnico $tecnico): self
    {
        $this->tecnico = $tecnico;

        return $this;
    }

    public function getVehiculo(): ?Vehiculo
    {
        return $this->vehiculo;
    }

    public function setVehiculo(?Vehiculo $vehiculo): self
    {
        $this->vehiculo = $vehiculo;

        return $this;
    }


}
