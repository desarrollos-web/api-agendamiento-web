<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Vehiculo
 *
 * @ORM\Table(name="vehiculo", indexes={@ORM\Index(name="FK_vehiculo_cliente", columns={"cliente_id"})})
 * @ORM\Entity
 */
class Vehiculo
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="placa", type="string", length=10, nullable=true, options={"default"="NULL"})
     */
    private $placa = 'NULL';

    /**
     * @var string|null
     *
     * @ORM\Column(name="modelo", type="string", length=20, nullable=true, options={"default"="NULL"})
     */
    private $modelo = 'NULL';

    /**
     * @var string|null
     *
     * @ORM\Column(name="anio", type="string", length=20, nullable=true, options={"default"="NULL"})
     */
    private $anio = 'NULL';

    /**
     * @var \Cliente
     *
     * @ORM\ManyToOne(targetEntity="Cliente")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="cliente_id", referencedColumnName="id")
     * })
     */
    private $cliente;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPlaca(): ?string
    {
        return $this->placa;
    }

    public function setPlaca(?string $placa): self
    {
        $this->placa = $placa;

        return $this;
    }

    public function getModelo(): ?string
    {
        return $this->modelo;
    }

    public function setModelo(?string $modelo): self
    {
        $this->modelo = $modelo;

        return $this;
    }

    public function getAnio(): ?string
    {
        return $this->anio;
    }

    public function setAnio(?string $anio): self
    {
        $this->anio = $anio;

        return $this;
    }

    public function getCliente(): ?Cliente
    {
        return $this->cliente;
    }

    public function setCliente(?Cliente $cliente): self
    {
        $this->cliente = $cliente;

        return $this;
    }


}
