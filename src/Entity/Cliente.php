<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cliente
 *
 * @ORM\Table(name="cliente", indexes={@ORM\Index(name="FK_cliente_general", columns={"tipo_doc"})})
 * @ORM\Entity
 */
class Cliente
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="apellidos", type="string", length=50, nullable=true, options={"default"="NULL"})
     */
    private $apellidos = 'NULL';

    /**
     * @var string|null
     *
     * @ORM\Column(name="nombres", type="string", length=50, nullable=true, options={"default"="NULL"})
     */
    private $nombres = 'NULL';

    /**
     * @var string|null
     *
     * @ORM\Column(name="documento", type="string", length=20, nullable=true, options={"default"="NULL"})
     */
    private $documento = 'NULL';

    /**
     * @var string|null
     *
     * @ORM\Column(name="numero_contacto", type="string", length=50, nullable=true, options={"default"="NULL"})
     */
    private $numeroContacto = 'NULL';

    /**
     * @var string|null
     *
     * @ORM\Column(name="correo", type="string", length=50, nullable=true, options={"default"="NULL"})
     */
    private $correo = 'NULL';

    /**
     * @var \General
     *
     * @ORM\ManyToOne(targetEntity="General")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tipo_doc", referencedColumnName="id")
     * })
     */
    private $tipoDoc;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getApellidos(): ?string
    {
        return $this->apellidos;
    }

    public function setApellidos(?string $apellidos): self
    {
        $this->apellidos = $apellidos;

        return $this;
    }

    public function getNombres(): ?string
    {
        return $this->nombres;
    }

    public function setNombres(?string $nombres): self
    {
        $this->nombres = $nombres;

        return $this;
    }

    public function getDocumento(): ?string
    {
        return $this->documento;
    }

    public function setDocumento(?string $documento): self
    {
        $this->documento = $documento;

        return $this;
    }

    public function getNumeroContacto(): ?string
    {
        return $this->numeroContacto;
    }

    public function setNumeroContacto(?string $numeroContacto): self
    {
        $this->numeroContacto = $numeroContacto;

        return $this;
    }

    public function getCorreo(): ?string
    {
        return $this->correo;
    }

    public function setCorreo(?string $correo): self
    {
        $this->correo = $correo;

        return $this;
    }

    public function getTipoDoc(): ?General
    {
        return $this->tipoDoc;
    }

    public function setTipoDoc(?General $tipoDoc): self
    {
        $this->tipoDoc = $tipoDoc;

        return $this;
    }


}
