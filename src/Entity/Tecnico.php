<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tecnico
 *
 * @ORM\Table(name="tecnico", indexes={@ORM\Index(name="FK_tecnico_general", columns={"tipo_doc"}), @ORM\Index(name="FK_tecnico_general_esp", columns={"especialidad"})})
 * @ORM\Entity
 */
class Tecnico
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="documento", type="string", length=20, nullable=true, options={"default"="NULL"})
     */
    private $documento = 'NULL';

    /**
     * @var string|null
     *
     * @ORM\Column(name="nombre", type="string", length=50, nullable=true, options={"default"="NULL"})
     */
    private $nombre = 'NULL';

    /**
     * @var \General
     *
     * @ORM\ManyToOne(targetEntity="General")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="tipo_doc", referencedColumnName="id")
     * })
     */
    private $tipoDoc;

    /**
     * @var \General
     *
     * @ORM\ManyToOne(targetEntity="General")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="especialidad", referencedColumnName="id")
     * })
     */
    private $especialidad;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDocumento(): ?string
    {
        return $this->documento;
    }

    public function setDocumento(?string $documento): self
    {
        $this->documento = $documento;

        return $this;
    }

    public function getNombre(): ?string
    {
        return $this->nombre;
    }

    public function setNombre(?string $nombre): self
    {
        $this->nombre = $nombre;

        return $this;
    }

    public function getTipoDoc(): ?General
    {
        return $this->tipoDoc;
    }

    public function setTipoDoc(?General $tipoDoc): self
    {
        $this->tipoDoc = $tipoDoc;

        return $this;
    }

    public function getEspecialidad(): ?General
    {
        return $this->especialidad;
    }

    public function setEspecialidad(?General $especialidad): self
    {
        $this->especialidad = $especialidad;

        return $this;
    }


}
